import numeral from "numeral";

import {
  TTypografikBreakpoint,
  TTypografikUtils,
  TTypografikContext,
  TCssProperty,
} from "./types";

export default (
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext
): TTypografikUtils => {
  const str = (n: number) => numeral(n).format("0.[0000]");

  const pixel = context.config.customProperties
    ? (n: number) => `calc(${str(n / breakpoint.baseline)} * var(--baseline))`
    : context.config.useRems
    ? (n: number) => `${str(n / 16)}rem`
    : (n: number) => `${str(n)}px`;

  const baselines = context.config.customProperties
    ? (n: number) => `calc(${str(n)} * var(--baseline))`
    : context.config.useRems
    ? (n: number) => `${str((n * breakpoint.baseline) / 16)}rem`
    : (n: number) => `${str(n * breakpoint.baseline)}px`;

  const cssProperty = ([n, type]: TCssProperty) =>
    type === "pixel" ? pixel(n) : baselines(n);

  const baseline = context.config.useRems
    ? `${str(breakpoint.baseline / 16)}rem`
    : `${str(breakpoint.baseline)}px`;

  return { pixel, baselines, cssProperty, baseline };
};
