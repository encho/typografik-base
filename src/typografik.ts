import { css as beautifyCss, css_beautify } from "js-beautify";
import capsize from "capsize";
import numeral from "numeral";
import {
  TTypografikTypeset,
  TTypografikBreakpoint,
  TTypografikContext,
  TTypografikUtils,
  TTypografikFontStyles,
} from "./types";

import toCapsize from "./toCapsize";
import getUtils from "./getUtils";
import aasciArt from "./aasciArt";
import generateUtilsCSS from "./generateUtilsCSS";

const str = (n: number) => numeral(n).format("0.[0000]");

// https://utopia.fyi/

export function generateFontStyles(
  typeset: TTypografikTypeset,
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext,
  utils: TTypografikUtils
): TTypografikFontStyles {
  const capsizeStyles = toCapsize(typeset, breakpoint, context);

  const isCapSizeFull = typeset.size[0] % 1 === 0;

  const lineGapInPixel = utils.pixel(breakpoint.baseline * typeset.space[0]);
  const lineGapInRem = utils.baselines(typeset.space[0]);
  const lineGap = context.config.useRems ? lineGapInRem : lineGapInPixel;

  const pixelToNumber = (x: string) => parseFloat(x.split("px")[0]);

  const lineHeight = context.config.withFractionalLineHeight
    ? str(
        pixelToNumber(capsizeStyles.lineHeight) /
          pixelToNumber(capsizeStyles.fontSize)
      )
    : utils.pixel(pixelToNumber(capsizeStyles.lineHeight));

  const capsizeStylesForFontStyle = {
    fontSize: utils.pixel(pixelToNumber(capsizeStyles.fontSize)),
    lineHeight,
    padding: `${utils.pixel(pixelToNumber(capsizeStyles.padding))} 0`,
  };

  const before = capsizeStyles["::before"];
  const after = capsizeStyles["::after"];

  if (context.config.withCapsizeTrim) {
    return {
      font: {
        color: "inherit",
        fontFamily: typeset.fontFamily,
        fontWeight: typeset.fontWeight || "normal",
        fontSize: capsizeStylesForFontStyle.fontSize,
        lineHeight: capsizeStylesForFontStyle.lineHeight,
        padding: capsizeStylesForFontStyle.padding,
        maxWidth: typeset.maxWidth
          ? `${typeset.maxWidth[0]}${typeset.maxWidth[1]}`
          : "auto",
      },
      before: {
        content: before.content,
        display: before.display,
        height: before.height,
        marginTop: before.marginTop,
      },
      after: {
        content: after.content,
        display: after.display,
        height: typeset.forceGrid && !isCapSizeFull ? lineGap : after.height,
        marginBottom: after.marginBottom,
      },
    };
  }

  return {
    font: {
      color: "inherit",
      fontFamily: typeset.fontFamily,
      fontWeight: typeset.fontWeight || "regular",
      fontSize: capsizeStylesForFontStyle.fontSize,
      lineHeight: capsizeStylesForFontStyle.lineHeight,
      padding: capsizeStylesForFontStyle.padding,
      maxWidth: typeset.maxWidth
        ? `${typeset.maxWidth[0]}${typeset.maxWidth[1]}`
        : "auto",
    },
  };
}

export function generateNormalFontStyles(
  typeset: TTypografikTypeset,
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext,
  utils: TTypografikUtils
): TTypografikFontStyles {
  let font = context.fonts.find((font) => font.name === typeset.fontFamily);

  if (!font) {
    console.warn("Font not found!");
  }

  font = context.fonts[0];

  // const { fontMetrics } = font;
  const { baseline } = breakpoint;

  const [size, sizeType] = typeset.size;
  const [space, spaceType] = typeset.space;

  if (sizeType === "fontSize" && spaceType === "leading") {
    const fontSize = utils.pixel(size * baseline);
    // const lineHeight = utils.pixel(space * baseline);

    const lineHeight = context.config.withFractionalLineHeight
      ? str(space / size)
      : utils.pixel(space * baseline);

    const CSSinJS = {
      font: {
        color: "inherit",
        fontFamily: typeset.fontFamily,
        fontWeight: typeset.fontWeight || "regular",
        fontSize,
        lineHeight,
        maxWidth: typeset.maxWidth
          ? `${typeset.maxWidth[0]}${typeset.maxWidth[1]}`
          : "auto",
      },
    };

    return CSSinJS;
  } else {
    throw Error(
      "Config allows only for sizeType of 'fontSize' and spaceType of 'lineHeight' types"
    );
  }
}

type TCreateCssProps = {
  selector: string;
  fontStyles: TTypografikFontStyles;
};

function createCSS({ fontStyles, selector }: TCreateCssProps) {
  // color: ${fontStyles.font.color}; // TODO add back?????

  const font = fontStyles.font
    ? `
  ${selector} {
    font-size: ${fontStyles.font.fontSize};
    line-height: ${fontStyles.font.lineHeight};${
        fontStyles?.font?.padding
          ? `padding: ${fontStyles?.font?.padding};`
          : ""
      }font-family: ${fontStyles.font.fontFamily};
    font-weight: ${fontStyles.font.fontWeight};${
        fontStyles?.font?.maxWidth === "auto"
          ? ""
          : `max-width: ${fontStyles.font.maxWidth};`
      } 
  } 
  `
    : "";

  const before = fontStyles.before
    ? `
  ${selector}:before {
    content: ${fontStyles.before.content};
    display: ${fontStyles.before.display};
    height: ${fontStyles.before.height};
    margin-top: ${fontStyles.before.marginTop};
} 
  `
    : "";

  const after = fontStyles.after
    ? `
  ${selector}:after {
    content: ${fontStyles.after.content};
    display: ${fontStyles.after.display};
    height: ${fontStyles.after.height};
    margin-bottom: ${fontStyles.after.marginBottom};
} 
  `
    : "";

  const rawCSS = `
    ${font}
    ${before}
    ${after}
  `;

  return beautifyCss(rawCSS);
}

export function generateTypesetStyleData(
  typeset: TTypografikTypeset,
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext,
  utils: TTypografikUtils
) {
  const selector = `.${typeset.className}`;

  const fontStyles = context.config.withCapsize
    ? generateFontStyles(typeset, breakpoint, context, utils) // TODO rename to getCapsizeFontStyles()
    : generateNormalFontStyles(typeset, breakpoint, context, utils); // TODO rename to getFontStyles()

  return { selector, fontStyles };
}

function generateTypesetsStylesData(
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext,
  utils: TTypografikUtils
) {
  return breakpoint.typesets.map((typeset) =>
    generateTypesetStyleData(typeset, breakpoint, context, utils)
  );
}

function generateTypesetsCSS(
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext,
  utils: TTypografikUtils
): string {
  const stylesData = generateTypesetsStylesData(breakpoint, context, utils);
  const cssFromDataArray = stylesData.map((styleItem) => createCSS(styleItem));
  const rawCSS = cssFromDataArray.reduce(
    (memo, currentCSS) => `${memo} \n ${currentCSS}`,
    ""
  );

  return rawCSS;
}

type THtmlStylesObject = {
  selector: "html";
  style: {
    fontSize?: string;
    "--baseline"?: string;
  };
};

const getHtmlStyles = (
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext,
  utils: TTypografikUtils
) => {
  const styles: THtmlStylesObject = {
    selector: "html",
    style: {},
  };

  if (context.config.customProperties) {
    styles.style["--baseline"] = utils.baseline;
    // styles.style['--containerMaxWidth'] = utils.cssProperty(breakpoint.max); // TODO reenable
  }

  if (context.config.accessibility || context.config.useRems) {
    styles.style.fontSize = context.config.accessibility ? "100%" : "16px";
  }

  return styles;
};

const generateHtmlBaseCSSProperties = (
  htmlStyles: any,
  context: TTypografikContext
) => {
  console.log("********");
  console.log("********");
  console.log("********");
  console.log("********");
  console.log(JSON.stringify(htmlStyles));
  console.log("********");
  console.log("********");
  console.log("********");
  console.log("********");

  let css = "";
  if (htmlStyles.style.fontSize) {
    css = css + `font-size: ${htmlStyles.style.fontSize};`;
  }
  if (htmlStyles.style["--baseline"]) {
    css = css + `--baseline: ${htmlStyles.style["--baseline"]};`;
  }

  // todo reenable??
  // if (htmlStyles["--containerMaxWidth"]) {
  //   css =
  //     css + `--containerMaxWidth: ${htmlStyles.style["--containerMaxWidth"]};`;
  // }

  return css;
};

const getResponsiveMediaQuery = (breakpoint: TTypografikBreakpoint) => {
  return `@media only screen and (min-width: ${breakpoint.start[0]}px) /* ---- ${breakpoint.name} ---- */`;
};

const setupTypography = (
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext
): string => {
  const utils = getUtils(breakpoint, context);

  const htmlStyles = getHtmlStyles(breakpoint, context, utils);

  const baselinesCustomProperties = context.config.customProperties
    ? `/* Shorthand css variables for baseline multiples */
        --baseline0: ${utils.baselines(0)};
--baseline1: ${utils.baselines(1)};
--baseline2: ${utils.baselines(2)};
--baseline3: ${utils.baselines(3)};
--baseline4: ${utils.baselines(4)};
--baseline5: ${utils.baselines(5)};
--baseline6: ${utils.baselines(6)};
--baseline7: ${utils.baselines(7)};
--baseline8: ${utils.baselines(8)};
--baseline9: ${utils.baselines(9)};
--baseline10: ${utils.baselines(10)};
--baseline11: ${utils.baselines(11)};
--baseline12: ${utils.baselines(12)};
 `
    : "";

  const spacingSystemCustomProperties = context.config.customProperties
    ? `/* Shorthand css variables for spacing system units */
    --space0: ${utils.baselines(0)};
    --space1: ${utils.baselines(1)};
    --space2: ${utils.baselines(2)};
    --space3: ${utils.baselines(3)};
    --space4: ${utils.baselines(5)};
    --space5: ${utils.baselines(8)};
    --space6: ${utils.baselines(13)};
    --space7: ${utils.baselines(21)};
    --space8: ${utils.baselines(34)};
    --space9: ${utils.baselines(55)};
    --space10: ${utils.baselines(89)};`
    : "";

  const customProperties = ""; // TODO reenable
  // const customProperties =
  //   baselinesCustomProperties + spacingSystemCustomProperties;

  const generateHtmlCSS = (xxx: THtmlStylesObject) => {
    if (
      Object.keys(htmlStyles.style).length ||
      context.config.customProperties
    ) {
      return `${xxx.selector} {
        ${generateHtmlBaseCSSProperties(htmlStyles, context)}${customProperties}
    }`;
    }
    return "";
  };

  return `
      ${context.config.showTypografikBrand ? aasciArt : ""}
/*
${context.info.description}
*/

      ${generateHtmlCSS(htmlStyles)}

      /* typography */
      /* ----------------------------------- */${generateTypesetsCSS(
        breakpoint,
        context,
        utils
      )}


      /* utils */
      /* ----------------------------------- */${generateUtilsCSS(
        breakpoint,
        context,
        utils
      )}

    `;
};

const responsiveTypography = (
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext
): string => {
  const utils = getUtils(breakpoint, context);
  const htmlStyles = getHtmlStyles(breakpoint, context, utils);

  const responsiveTypographyCSS = `
  
    ${getResponsiveMediaQuery(breakpoint)} {
      ${htmlStyles.selector} {
        ${generateHtmlBaseCSSProperties(htmlStyles, context)}
      }
      ${generateTypesetsCSS(breakpoint, context, utils)}
    }
  `;

  // console.log(beautifyCss(responsiveTypographyCSS));

  return responsiveTypographyCSS;
};

const generateTypografikCSS = (context: TTypografikContext) => {
  const { breakpoints } = context;
  const [firstBreakpoint, ...otherBreakpoints] = breakpoints;
  const rawCSS = `
   ${setupTypography(firstBreakpoint, context)}
   ${otherBreakpoints.reduce((memo, breakpoint) => {
     return `${memo} ${responsiveTypography(breakpoint, context)}`;
   }, "")}
 `;

  const formattedCSS = beautifyCss(rawCSS);
  // console.log(formattedCSS);
  return formattedCSS;
};

export default generateTypografikCSS;
