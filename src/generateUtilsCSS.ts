import { css as beautifyCss, css_beautify } from "js-beautify";
import capsize from "capsize";
import numeral from "numeral";
import {
  TTypografikTypeset,
  TTypografikBreakpoint,
  TTypografikContext,
  TTypografikUtils,
  TTypografikFontStyles,
} from "./types";

import toCapsize from "./toCapsize";
import getUtils from "./getUtils";
import aasciArt from "./aasciArt";

const str = (n: number) => numeral(n).format("0.[0000]");

// https://utopia.fyi/

const BASELINE_COLOR = "#E79C7D";
const TEXT_BOUNDING_BOX_COLOR = "rgba(231, 156, 125, 0.50)";
const BUTTON_BACKGROUND_COLOR = "#E79C7D";
const BUTTON_COLOR = "#222222";

export default function generateUtilsCSS(
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext,
  utils: TTypografikUtils
): string {
  const utilsCSS = `
    .typography-section {
      width: 100%;
    }

    .typography-container {
      margin: 0 auto;
      max-width: ${utils.cssProperty(breakpoint.max)};
    }

    .typography-baseline-grid {
      position: absolute;
      z-index: 100;
      display: block;
      width: auto;
      height: auto;
      top: -1px;
      left: 0;
      bottom: 0;
      right: 0;
      pointer-events: none;
      background-repeat: repeat;
      background-size: 100% ${utils.baselines(1)};
      background-position: top left;
      background-image: linear-gradient(${BASELINE_COLOR} 0px, transparent 1px, transparent 100%);
    }

    .btn {
      background-color: ${BUTTON_BACKGROUND_COLOR};
      color: ${BUTTON_COLOR};
      padding: ${utils.baselines(3)} ${utils.baselines(3)};
      border-radius: 3px;
      min-width: 120px;
      border: none;
    }



    ul {
      list-style-type: none;
      list-style-image: none;
      list-style-position: inside;
    }


    /* TODO use body typeset leading to determine these sizes */
    .typography-body-leading-height {
      height: ${utils.baselines(3)};
    }

    li + li {
      margin-top: ${utils.baselines(5)};
    }

    .typography-info-section {
      // background-color: red;
    }

    .typography-info-section + .typography-info-section {
      margin-top: ${utils.baselines(10)};
    }

    .bg-white {
      background-color: #f5f5f5;
    }

    .bg-black {
      background-color: #000000;
    }

    .fill-black {
      fill: #000000;
    }

    .color-black {
      color: #000000;
    }

    .bg-inspect {
      background-color: ${TEXT_BOUNDING_BOX_COLOR};
    }

    .p-8 {
      padding: ${utils.baselines(8)};
    } 

    .mt-baselines-20 {
      margin-top: ${utils.baselines(20)};
    }

    .mb-baselines-20 {
      margin-bottom: ${utils.baselines(20)};
    }

    .pt-baselines-20 {
      padding-top: ${utils.baselines(20)};
    }

    .pb-baselines-20 {
      padding-bottom: ${utils.baselines(20)};
    }

    .mb-baselines-2 {
      margin-bottom: ${utils.baselines(2)};
    }

    .mb-baselines-10 {
      margin-bottom: ${utils.baselines(10)};
    }

    .mb-baselines-6 {
      margin-bottom: ${utils.baselines(6)};
    }


    .pt-baselines-6 {
      padding-top: ${utils.baselines(6)};
    }

    .pb-baselines-6 {
      padding-bottom: ${utils.baselines(6)};
    }

    .mr-baselines-1 {
      margin-right: ${utils.baselines(1)}
    }

    .mr-baselines-2 {
      margin-right: ${utils.baselines(2)}
    }

    .mt-1 {
      margin-top: ${utils.baselines(1)};
    }

    .mt-2 {
      margin-top: ${utils.baselines(2)};
    }

    .mt-3 {
      margin-top: ${utils.baselines(3)};
    }

    .mt-4 {
      margin-top: ${utils.baselines(5)};
    }

    .mt-5 {
      margin-top: ${utils.baselines(8)};
    }

    .mt-6 {
      margin-top: ${utils.baselines(13)};
    }

    .mt-7 {
      margin-top: ${utils.baselines(21)};
    }

    .mb-1 {
      margin-bottom: ${utils.baselines(1)};
    }

    .mb-2 {
      margin-bottom: ${utils.baselines(2)};
    }

    .mb-3 {
      margin-bottom: ${utils.baselines(3)};
    }

    .mb-4 {
      margin-bottom: ${utils.baselines(5)};
    }

    .mb-5 {
      margin-bottom: ${utils.baselines(8)};
    }

    .mb-6 {
      margin-bottom: ${utils.baselines(13)};
    }

    .mb-7 {
      margin-bottom: ${utils.baselines(21)};
    }

    .pt-1 {
      padding-top: ${utils.baselines(1)};
    }

    .pt-2 {
      padding-top: ${utils.baselines(2)};
    }

    .pt-3 {
      padding-top: ${utils.baselines(3)};
    }

    .pt-4 {
      padding-top: ${utils.baselines(5)};
    }

    .pt-5 {
      padding-top: ${utils.baselines(8)};
    }

    .pt-6 {
      padding-top: ${utils.baselines(13)};
    }

    .pt-7 {
      padding-top: ${utils.baselines(21)};
    }

    .pb-1 {
      padding-bottom: ${utils.baselines(1)};
    }

    .pb-2 {
      padding-bottom: ${utils.baselines(2)};
    }

    .pb-3 {
      padding-bottom: ${utils.baselines(3)};
    }

    .pb-4 {
      padding-bottom: ${utils.baselines(5)};
    }

    .pb-5 {
      padding-bottom: ${utils.baselines(8)};
    }

    .pb-6 {
      padding-bottom: ${utils.baselines(13)};
    }

    .pb-7 {
      padding-bottom: ${utils.baselines(21)};
    }
  `;

  return utilsCSS;
}
