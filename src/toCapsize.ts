import capsize from "capsize";
import { css as beautifyCss } from "js-beautify";
import {
  TTypografikTypeset,
  TTypografikBreakpoint,
  TTypografikContext,
} from "./types";

// https://utopia.fyi/

export default function toCapsize(
  typeset: TTypografikTypeset,
  breakpoint: TTypografikBreakpoint,
  context: TTypografikContext
) {
  let font = context.fonts.find((font) => font.name === typeset.fontFamily);

  if (!font) {
    console.warn("Font not found!");
  }

  font = context.fonts[0];

  const { fontMetrics } = font;
  const { baseline } = breakpoint;

  const [size, sizeType] = typeset.size;
  const [space, spaceType] = typeset.space;
  let capsizeOptions;
  if (sizeType === "capHeight" && spaceType === "lineGap") {
    return capsize({
      fontMetrics,
      capHeight: size * baseline,
      lineGap: space * baseline,
    });
    // capsizeOptions = {
    //   fontMetrics,
    //   capHeight: size * baseline,
    //   lineGap: space * baseline,
    // };
  } else if (sizeType === "capHeight" && spaceType === "leading") {
    return capsize({
      fontMetrics,
      capHeight: size * baseline,
      leading: space * baseline,
    });
    // capsizeOptions = {
    //   fontMetrics,
    //   capHeight: size * baseline,
    //   leading: space * baseline,
    // };
  } else if (sizeType === "fontSize" && spaceType === "lineGap") {
    return capsize({
      fontMetrics,
      fontSize: size * baseline,
      lineGap: space * baseline,
    });
    // capsizeOptions = {
    //   fontMetrics,
    //   fontSize: size * baseline,
    //   lineGap: space * baseline,
    // };
  } else if (sizeType === "fontSize" && spaceType === "leading") {
    return capsize({
      fontMetrics,
      fontSize: size * baseline,
      leading: space * baseline,
      // lineGap: (space - size) * baseline,
    });
    // capsizeOptions = {
    //   fontMetrics,
    //   fontSize: size * baseline,
    //   leading: space * baseline,
    //   // lineGap: (space - size) * baseline,
    // };
  }

  throw new Error("Wrong inputs to toCapsize");
  //   return capsize(capsizeOptions);
}
