// see https://github.com/TodayDesign/megatype
// start is the start position of the breakpoint. Can be px or em.
// max is the max width of the container. Can be px, em, or %.
// rootsize is the base font size applied to the html element. Can be px or rem. This also controls our grid size at this breakpoint. These values can be retrieved using the break-get function, eg:

export type TTypografikTypeset = {
  fontFamily: string;
  size: [number, "capHeight" | "fontSize"];
  space: [number, "lineGap" | "leading"];
  className: string;
  maxWidth?: [number, "em" | "ch"];
  fontWeight?: number;
  forceGrid?: boolean;
};

export type TTypografikBreakpoint = {
  start: [number, "px"] | [number, "em"];
  max: [number, "pixel"] | [number, "baselines"];
  baseline: number;
  name: string;
  typesets: TTypografikTypeset[];
};

export type TTypografikFont = {
  name: string;
  fontMetrics: {
    capHeight: number;
    ascent: number;
    descent: number;
    lineGap: number;
    unitsPerEm: number;
  };
};

export type TTypografikConfig = {
  accessibility: boolean;
  useRems: boolean;
  withFractionalLineHeight: boolean;
  withCapsize: boolean;
  withCapsizeTrim: boolean;
  customProperties: boolean;
  showTypografikBrand: boolean;
};

export type TTypografikContext = {
  breakpoints: TTypografikBreakpoint[];
  fonts: TTypografikFont[];
  config: TTypografikConfig;
  info: {
    description: string;
  };
};

type TSizeType = "baselines" | "pixel";
export type TCssProperty = [number, TSizeType];

export type TTypografikUtils = {
  pixel: (nrPixel: number) => string; // CSS Property Value
  baselines: (nrBaselines: number) => string; // CSS Property Value
  cssProperty: (property: TCssProperty) => string; // CSS Property Value
  baseline: string; // CSS Property Value
};

export type TTypografikFontStyles = {
  font: {
    color: string;
    fontFamily: string; // or better Array<TFont>
    fontWeight: number | string;
    fontSize: string;
    lineHeight: string;
    padding?: string;
    maxWidth: string;
  };
  before?: {
    content: string;
    display: string;
    height: string | number;
    marginTop: string;
  };
  after?: {
    content: string;
    display: string;
    height: string | number;
    marginBottom: string;
  };
};
