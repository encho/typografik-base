# Typescript Base

This package provides basic utilities for specifying and working with Typografik configuration objects.

E.g.:

- Basic types
- JS styles creation
- CSS printing

## Testing

Run `yarn test`

## Publishing

Run `npm publish`

## Development

If you want to verify that it works functionally before you publish (this is a good idea), then in your package root, run npm link. Then go to another folder and npm link <your package name>. Go into a .ts file and import your package by name (just like you would normally if you had npm installed it). You should see your types coming through in your editor.